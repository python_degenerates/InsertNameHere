# We still don't have a name, lol

## How to do stuff

```bash
# First you want to add your current changes. This can be done with
git add .

# Then you need to need to commit your thing
git commit -m "My commit message should go here"

# Then fetch the most recent changes from the repo and the merge them into local
git fetch
git merge origin/dev

# Now you can push to dev!
git push origin dev

```

## Branches

When you first clone the repository, it will default to the `Master` branch. All of our major coding will happen
in the `dev` branch and its derivatives. In order to switch to the `dev` branch

### Master

The master branch is our demo branch. Only code that we're going to demo/turn in are going to go into this branch.
I'll take care of branch merging. It can get annoying and confusing and mistakes can happen, and I'd rather be the one
to make the big mistakes. I know what it's like to fuck up a repo and it's not fun, especially if you don't know how
to fix it.

### dev

In progress code goes here. It must compile, but it doesn't have to work. We'll probably have more branches from this
one for each person in the group or for each feature. Should be pretty straight forward.

### v{insert_version_number}

Branches with this naming convention will contain our old demos. Once we have demoed the code in master, we're going to
create a new branch from master to archive our progress at that point in case we ever need to scrap a ton of code. It's
easier just to switch to another branch than it is to undo all the commits.
