package python_degenerates.degenerats;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Clare on 11/12/17.
 */

public class ClareTest {

    private String mStringToBetyped;

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(
            LoginActivity.class);

    @Before
    public void initValidString() {
        // Specify a valid string.
        mStringToBetyped = "Welcome";
    }

    @Test
    public void changeText_sameActivity() {
        // Type text and then press the button.
        onView(withId(R.id.email))
                .perform(typeText("claretrively@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.password))
                .perform(typeText("claretrively"), closeSoftKeyboard());
        DatabaseManager.loadInitialData();
        while(UserManager.getUsers() == null);
        while(SightingManager.getSightings() == null);
        onView(withId(R.id.email_sign_in_button)).perform(click());
        while(LoginActivity.getUser() == null);
        Assert.assertNotNull(LoginActivity.getUser());

        // Check that the text was changed.
        //onView(withId(R.id.welcome_text))
           //     .check(matches(withText(mStringToBetyped)));
    }
}
