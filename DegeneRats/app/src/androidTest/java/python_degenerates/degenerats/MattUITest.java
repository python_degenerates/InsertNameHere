package python_degenerates.degenerats;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.doubleClick;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.endsWith;

/**
 * Created by mattmcauliffe on 11/10/17.
 */

@RunWith(AndroidJUnit4.class)
public class MattUITest {

    /* Instantiate an IntentsTestRule object. */
    @Rule
    public IntentsTestRule<HomeScreenActivity> mIntentsRule =
            new IntentsTestRule<>(HomeScreenActivity.class);

    @Test
    public void verifyMessageSentToMessageActivity() {

        // Clicks a button to send the message to another
        // activity through an explicit intent.
        onView(withId(R.id.goToLoginButton)).perform(click());

        // Verifies that the LoginActivity received an intent
        intended(allOf(
                hasComponent(hasShortClassName(".LoginActivity"))));


    }
    @Test
    public void testIntroUI() {
        onView(withId(R.id.goToLoginButton)).perform(click());

        // Verifies that the LoginActivity received an intent
        intended(allOf(
                hasComponent(hasShortClassName(".LoginActivity"))));
        onView(withId(R.id.email)).perform(typeText("m@test.com"));
        onView(withId(R.id.password)).perform(typeText("password"), closeSoftKeyboard());
        while (SightingManager.getSightings() == null);
        while (UserManager.getUsers() == null);
        onView(withId(R.id.email_sign_in_button)).perform(click());
    }
}

