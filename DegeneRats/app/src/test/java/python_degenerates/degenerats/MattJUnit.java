package python_degenerates.degenerats;

/**
 * Created by mattmcauliffe on 11/9/17.
 */





import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.*;

public class MattJUnit {

    @Test
    public void testCreateAdmin() {
        User newTestUser = new User("Matt", User.UserType.ADMIN, "matt@test.com");
        assertEquals(newTestUser.getName(),"Matt");
        assertEquals(newTestUser.getUserType(),User.UserType.ADMIN);
        assertEquals(newTestUser.getUserEmail(),"matt@test.com");
        assertEquals(newTestUser.isBlocked(), false);
        newTestUser.setBlocked(true);
        assertEquals(newTestUser.isBlocked(), true);
    }
    @Test
    public void testCreateUser() {
        User newTestUser = new User("Matt", User.UserType.USER, "matt@test.com");
        assertEquals(newTestUser.getName(),"Matt");
        assertEquals(newTestUser.getUserType(),User.UserType.USER);
        assertEquals(newTestUser.getUserEmail(),"matt@test.com");
        assertEquals(newTestUser.isBlocked(), false);
        newTestUser.setBlocked(true);
        assertEquals(newTestUser.isBlocked(), true);
    }
    @Test
    public void testGetUsers() {
        assertEquals(UserManager.getUsers(), null);
        ArrayList<User> newUsers = new ArrayList<>();
        newUsers.add(new User("Matt", User.UserType.USER, "matt@test.com"));
        UserManager.setUsers(newUsers);
        assertNotEquals(UserManager.getUsers(), null);
    }

}
