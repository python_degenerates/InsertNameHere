package python_degenerates.degenerats;

import org.junit.Test;
import static org.junit.Assert.*;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
//used to turn long and lat into address -C
import android.location.Geocoder;
import android.location.Address;
import java.util.Locale;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;

/**
 * Created by chase on 11/9/17.
 */

public class ChaseTest {
    @Test
    public void testGetCompleteAddress() throws Exception {
        ReportLocationSelection locationClass = new ReportLocationSelection();

        LatLng testInput = new LatLng(40.70898692345805, -73.94120690238431);

        String expectedLocationType = "3+ Family Apt. Building";
        String expectedZip = "11206";
        String expectedAddress = "198 SCHOLES STREET";
        String expectedCity = "BROOKLYN";
        String expectedBorough = "BROOKLYN";
        String expectedLat = "40.70898692345805";
        String expectedLong = "-73.94120690238431";


        Address actualOutput = locationClass.getCompleteAddress(testInput);
        if (actualOutput == null) {
            //this means that the geocoder was unable to connect correctly to the
            //network
            assertEquals(actualOutput, null);
        } else {
            //if the geocoder was able to connect to the network
            assertEquals(actualOutput.getPostalCode(), expectedZip);
            assertEquals(actualOutput.getSubThoroughfare(), expectedLocationType);
            assertEquals(actualOutput.getThoroughfare(), expectedAddress);
            assertEquals(actualOutput.getLocale(), expectedCity);
            assertEquals(actualOutput.getSubLocality(), expectedBorough);
            assertEquals(actualOutput.getLatitude(), expectedLat);
            assertEquals(actualOutput.getLongitude(), expectedLong);
        }
    }
}
