package python_degenerates.degenerats;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Rajan Tayal's junit test for M10
 */

public class RJUnit {
    @Test
    public void illegalPasswordCharacters_isCorrect() throws Exception {
        LoginActivity tester = new LoginActivity();

        assertEquals(tester.illegalPasswordCharacters("password123"), true);
        assertEquals(tester.illegalPasswordCharacters(""), false);
    }
}
