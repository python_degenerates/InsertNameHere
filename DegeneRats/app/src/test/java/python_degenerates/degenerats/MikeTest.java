package python_degenerates.degenerats;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by mike on 11/9/17.
 */

public class MikeTest {
    @Test
    public void testEmailIsValid() throws Exception {
        LoginActivity testingBoi = new LoginActivity();

        String email1 = "mike@mdit.to";
        String email2 = "mcauliffe@gatech.edu";
        String email3 = "ctrively@gatech.edu";
        String email4 = "michael.r.ditto@gmail.com";

        String email5 = "asdfasdfasdf";
        String email6 = "michael.r.ditto.google";
        String email7 = "chase";
        String email8 = "word.com";

        assertEquals(testingBoi.isEmailValid(email1), true);
        assertEquals(testingBoi.isEmailValid(email2), true);
        assertEquals(testingBoi.isEmailValid(email3), true);
        assertEquals(testingBoi.isEmailValid(email4), true);

        assertEquals(testingBoi.isEmailValid(email5), false);
        assertEquals(testingBoi.isEmailValid(email6), false);
        assertEquals(testingBoi.isEmailValid(email7), false);
        assertEquals(testingBoi.isEmailValid(email8), false);
    }
}
