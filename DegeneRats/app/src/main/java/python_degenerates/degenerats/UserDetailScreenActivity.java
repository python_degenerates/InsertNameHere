package python_degenerates.degenerats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by mattmcauliffe on 9/2/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class UserDetailScreenActivity extends AppCompatActivity {

    @SuppressWarnings("FieldCanBeLocal")
    private FirebaseAuth mAuth;
    @SuppressWarnings("FieldCanBeLocal")
    private DatabaseReference mDatabase;

    /** this method lays out the user details for the admin on one screen
     *
     * @param savedInstanceState this bundle is used to create the view
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail_screen);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        Intent intent = getIntent();
        final int userPosition = intent.getIntExtra("Position", 0);
        List<User> users = UserManager.getUsers();
        final User user = users.get(userPosition);

        TextView fullName = findViewById(R.id.full_name_display);
        final TextView userName = findViewById(R.id.user_name_display);
        TextView blocked = findViewById(R.id.blocked_display);
        TextView userType = findViewById(R.id.user_type_display);
        fullName.setText(user.getName());
        userName.setText(user.getUserEmail());
        userType.setText(user.getUserType().toString());
        if (user.isBlocked()) {
            blocked.setText("Blocked");
        } else {
            blocked.setText("Unblocked");
        }

        //Button remove = findViewById(R.id.remove_user_button);
        Button block = findViewById(R.id.block_user_button);
        Button changeType = findViewById(R.id.change_type_button);
        Button cancel = findViewById(R.id.edit_user_cancel_button);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnToList = new Intent(UserDetailScreenActivity.this, ManageUsersActivity.class);
                startActivity(returnToList);
            }
        });

        changeType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserManager.changeUserType(user);
                startActivity(new Intent(UserDetailScreenActivity.this, ManageUsersActivity.class));
            }
        });

        block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserManager.blockUser(user);
                startActivity(new Intent(UserDetailScreenActivity.this, ManageUsersActivity.class));
            }
        });




    }
}
