package python_degenerates.degenerats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by mattmcauliffe on 9/2/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class SightingDetailScreenActivity extends AppCompatActivity {

    /** this method handles the elements of the sighting details screen after a user
     * clicks on a sighting from the main activity
     *
     * @param savedInstanceState this bundle is used to create the screen for sighting details
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sighting_detail_screen);

        Intent intent = getIntent();
        final int sightingPosition = intent.getIntExtra("Position", 0);

        List<String[]> sightings = SightingManager.getSightings();
        String[] sighting = sightings.get(sightingPosition);

        TextView id = findViewById(R.id.unique_key_display);
        TextView date = findViewById(R.id.created_date_display);
        TextView loc_type = findViewById(R.id.location_type_display);
        TextView inc_zip = findViewById(R.id.incident_zip_display);
        TextView inc_add = findViewById(R.id.incident_address_display);
        TextView city = findViewById(R.id.city_display);
        TextView borough = findViewById(R.id.borough_display);
        TextView lat = findViewById(R.id.latitude_display);
        TextView lon = findViewById(R.id.longitude_display);

        id.setText(sighting[0]);
        date.setText(sighting[1]);
        loc_type.setText(sighting[2]);
        inc_zip.setText(sighting[3]);
        inc_add.setText(sighting[4]);
        city.setText(sighting[5]);
        borough.setText(sighting[6]);
        if (sighting.length > 7) {
            lat.setText(sighting[7]);
            lon.setText(sighting[8]);
        }

        Button back = findViewById(R.id.back_button_sighting_report);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(SightingDetailScreenActivity.this, MainActivity.class);
                startActivity(back);
            }
        });

        Button viewOnMap = findViewById(R.id.view_button_sighting_report);
        viewOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Integer> positions = new ArrayList<>();
                positions.add(sightingPosition);
                Intent viewOnMap = new Intent(SightingDetailScreenActivity.this, SightingResultsMapsActivity.class);
                viewOnMap.putExtra("SightingPositions", positions);
                startActivity(viewOnMap);
            }
        });

    }
}
