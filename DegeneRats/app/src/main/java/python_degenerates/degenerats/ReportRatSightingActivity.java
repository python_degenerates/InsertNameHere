package python_degenerates.degenerats;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.location.Address;

/**
 * Created by mattmcauliffe on 9/2/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class ReportRatSightingActivity extends AppCompatActivity {

    @Override
    /**
     * This method sets up the report rat sighting activity screen. The text fields will be
     * populated with data from the passed in address object from reportLocationSelection if such
     * an intent was passed. The sighting manager is told to add the sighting and then an intent
     * is created to go back to the main menu.
     * @param savedInstanceState this state is used to create the view
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_rat_sighting);

        //a button on the top of the page that takes you to ReportLocationSelection so you can
        //retrieve location information from a map rather than type it in -C
        Button toReportLocation = findViewById(R.id.toReportLocationButton);
        toReportLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ReportRatSightingActivity.this,
                        ReportLocationSelection.class));
            }
        });

        EditText id = findViewById(R.id.unique_key_in_report);
        id.setText((SightingManager.getMaxID() + 1) + "");
        id.setEnabled(false);

        EditText date = findViewById(R.id.created_date_in_report);
        date.setText((new Date()).toString());
        date.setEnabled(false);

        //code to receive an intent from ReportLocationSelection
        //this intent should contain an extra with all of the different bits of an address
        //like zip code and long lat -C

        Intent addressBox = getIntent();
        if (addressBox.getParcelableExtra("Address") != null) {
            Address address = (Address)addressBox.getParcelableExtra("Address");
            //postal code -C
            ((EditText)findViewById(R.id.incident_zip_in_report)).setText(
                    address.getPostalCode());
            ((EditText)findViewById(R.id.incident_zip_in_report)).setEnabled(false);
            //city -C
            if (address.getLocality() == null) {
                ((EditText)findViewById(R.id.city_in_report)).setText(
                        address.getSubLocality());
                ((EditText)findViewById(R.id.city_in_report)).setEnabled(false);
            } else {
                ((EditText)findViewById(R.id.city_in_report)).setText(
                        address.getLocality());
                ((EditText)findViewById(R.id.city_in_report)).setEnabled(false);
            }
            //latitude -C
            ((EditText)findViewById(R.id.latitude_in_report)).setText(
                    ((Double)address.getLatitude()).toString());
            ((EditText)findViewById(R.id.latitude_in_report)).setEnabled(false);
            //longitude -C
            ((EditText)findViewById(R.id.longitude_in_report)).setText(
                    ((Double)address.getLongitude()).toString());
            ((EditText)findViewById(R.id.longitude_in_report)).setEnabled(false);
            //borough -C
            if (address.getSubLocality() == null) {
                ((EditText)findViewById(R.id.borough_in_report)).setText(
                        address.getLocality());
                ((EditText)findViewById(R.id.borough_in_report)).setEnabled(false);
            } else {
                ((EditText)findViewById(R.id.borough_in_report)).setText(
                        address.getSubLocality());
                ((EditText)findViewById(R.id.borough_in_report)).setEnabled(false);
            }
            //address -C
            if (address.getThoroughfare() != null && address.getSubThoroughfare() == null) {
                ((EditText)findViewById(R.id.incident_address_in_report)).setText(
                        address.getThoroughfare());
                ((EditText)findViewById(R.id.incident_address_in_report)).setEnabled(false);
            } else if (address.getThoroughfare() == null && address.getSubThoroughfare() != null) {
                //this conditional will probably never be used, but just in case. -C
                ((EditText)findViewById(R.id.incident_address_in_report)).setText(
                        address.getSubThoroughfare());
                ((EditText)findViewById(R.id.incident_address_in_report)).setEnabled(false);
            } else if (address.getThoroughfare() != null && address.getSubThoroughfare() != null) {
                ((EditText)findViewById(R.id.incident_address_in_report)).setText(
                        address.getSubThoroughfare() + " " + address.getThoroughfare());
                ((EditText)findViewById(R.id.incident_address_in_report)).setEnabled(false);
            }
        }

        //be able to go back to main activity
        Button cancel = findViewById(R.id.cancel_report_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ReportRatSightingActivity.this, MainActivity.class));
            }
        });

        Button submit = findViewById(R.id.submit_report_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //this intent takes us back to the main
                Intent submit = new Intent(ReportRatSightingActivity.this, MainActivity.class);
                //create an id
                String id = ((EditText)findViewById(R.id.unique_key_in_report)).getText().toString();

                //gets actual date/time
                Date theDate = new Date();
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
                String date = dateFormat.format(theDate);


                //this is all the stuff that was entered in
                String loc_type = ((EditText)findViewById(R.id.location_type_in_report)).getText().toString();
                String inc_zip = ((EditText)findViewById(R.id.incident_zip_in_report)).getText().toString();
                String inc_add = ((EditText)findViewById(R.id.incident_address_in_report)).getText().toString();
                String city = ((EditText)findViewById(R.id.city_in_report)).getText().toString();
                String borough = ((EditText)findViewById(R.id.borough_in_report)).getText().toString();
                String lat = ((EditText)findViewById(R.id.latitude_in_report)).getText().toString();
                String lon = ((EditText)findViewById(R.id.longitude_in_report)).getText().toString();

                // a sighting including all of the stuff above
                String[] newSighting = {id, date, loc_type, inc_zip, inc_add, city, borough, lat, lon};
                //putting this array into the interpreter
                //the interpreter is a method that takes a reference to a sighting and adds it to
                //the firebase )it might also add it to the sighting list)
                SightingManager.writeSightingData(newSighting);
                SightingManager.incrementMaxID();
                //starts the submit intent that takes us back to the class that we were in before
                startActivity(submit);
            }
        });
    }
}
