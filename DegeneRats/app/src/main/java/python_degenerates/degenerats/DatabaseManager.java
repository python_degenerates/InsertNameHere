package python_degenerates.degenerats;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mattmcauliffe on 10/19/17.
 */

@SuppressWarnings("CyclicClassDependency")
public class DatabaseManager {
    static DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    static FirebaseAuth mAuth = FirebaseAuth.getInstance();


    /**
     * Loads initial data of the database
     */
    public static void loadInitialData() {
        loadSightings();
        loadUsers();
    }

    /**
     * loads all reported rat sightings from the database
     */
    public static void loadSightings() {
        Query sightQuery = mDatabase.child("Sightings");
        //take what the query retrieved and it performs the actions in ONDataChange
        sightQuery.addValueEventListener(new ValueEventListener() {
            @Override

            //only run if the data was changed
            public void onDataChange(DataSnapshot dataSnapshot) {
                //if there are actually sightings
                if (dataSnapshot.exists()) {
                    Log.d("Getting Data", "Getting data from firebase");
                    //an array of sightings
                    ArrayList<String[]> data = new ArrayList<>();
                    //data snapshots can have datasnapshots as its children
                    //each sightings has a bunch of data so the datasnapshot of a sighting
                    //has a bunch of datasnapshots within it which are each piece of information
                    //about the sighting
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        String[] dataRow = new String[9];
                        int x = 0;
                        for (DataSnapshot item : snapshot.getChildren()) {
                            //for each piece of data in the sighting add it to the string for that sighting
                            dataRow[x] = (String) item.getValue();
                            if (x == 0) {
                                if (!((String)item.getValue()).equals("Unique Key")) {
                                    int idNum = Integer.parseInt((String)item.getValue());
                                    if (idNum > SightingManager.getMaxID()) {
                                        SightingManager.setMaxID(idNum);
                                    }
                                }
                            }
                            x++;
                        }
                        data.add(dataRow);
                    }
                    SightingManager.setSightings(data);
                }
            }


            @Override
            //if the query gets cancelled throw an error
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });
    }

    /**
     * loads all registered users from the database for logging in
     */
    public static void loadUsers() {
        Query userQuery = mDatabase.child("UserList");
        userQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()) {
                    ArrayList<User> newUserList = new ArrayList<>();

                    //turn database users back into a user object
                    for (DataSnapshot user : dataSnapshot.getChildren()) {
                        String typeString = (String) user.child("userType").getValue();
                        User.UserType type;
                        if (typeString.equals("ADMIN")) {
                            type = User.UserType.ADMIN;
                        } else {
                            type = User.UserType.USER;
                        }

                        String email = (String) user.child("userEmail").getValue();
                        String name = (String) user.child("name").getValue();
                        User newUser = new User(name, type, email);
                        boolean isBlocked = (Boolean) user.child("blocked").getValue();
                        newUser.setBlocked(isBlocked);
                        newUserList.add(newUser);
                    }
                    UserManager.setUsers(newUserList);
                }
            }

            /**
             * this method deals with an error produced by cancelling loading the database
             * @param databaseError error when loading cancelled
             */
            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });
    }
    private static boolean success;

    /**this method creates a new user in the database upon registration
     *
     * @param email users email
     * @param password users password
     * @param name user's name
     * @param type user type ie user or admin
     * @return boolean for created the user or not
     */
    public static boolean createUser(final String email, final String password, final String name, final User.UserType type) {

        OnCompleteListener<AuthResult> createListener = new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Create User Status", "createUserWithEmail:success");
                    //this .push is used to get a unique key (random letters) to add this as a
                    //child of user list
                    DatabaseReference newChildRef = mDatabase.getDatabase().getReference("UserList").push();
                    //create a new user from the field above
                    User user = new User(name, type, email);
                    //sets the value of what the reference above holds to the properties of user
                    newChildRef.setValue(user);
                    //go back to login activity
                    //we could just pass the email and password back in DIBS CHASE
                    success = true;


                } else {
                    //if the task was not successful give the poor sod a toast
                    // If sign in fails, display a message to the user.
                    success = false;

                }
            }
        };

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(createListener);
        return success;


    }

    /**marks a user as blocked so that they can no longer access the application
     *
     * @param user user to be blocked
     */
    public static void blockUser(final User user) {
        Query sightQuery = mDatabase.child("UserList");
        sightQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()) {
                    for (DataSnapshot userData : dataSnapshot.getChildren()) {
                        String email = (String) userData.child("userEmail").getValue();
                        if (email.equals(user.getUserEmail())) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("name", user.getName());
                            map.put("userEmail", user.getUserEmail());
                            map.put("userType", user.getUserType());
                            if (user.isBlocked()) {
                                map.put("blocked", false);
                            } else {
                                map.put("blocked", true);
                            }
                            userData.getRef().updateChildren(map);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });

    }

    /**
     * this method changes the type of a specified user
     * @param user user to have their status changed
     */
    public static void toggleUserType(final User user) {
        Query sightQuery = mDatabase.child("UserList");
        sightQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()) {
                    for (DataSnapshot userData : dataSnapshot.getChildren()) {
                        String email = (String) userData.child("userEmail").getValue();
                        if (email.equals(user.getUserEmail())) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("name", user.getName());
                            map.put("userEmail", user.getUserEmail());
                            if (user.getUserType().equals(User.UserType.USER)) {
                                map.put("userType", User.UserType.ADMIN);
                            } else if (user.getUserType().equals(User.UserType.ADMIN)) {
                                map.put("userType", User.UserType.USER);
                            }
                            userData.getRef().updateChildren(map);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });
    }


    /**this method arranges the data from a rat sighting
     *
     * @param sightingData data of the sighting
     */
    static void writeSightingData(String[] sightingData) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference newChildRef = mDatabase.getDatabase().getReference("Sightings/").push();
        List<String> data = new ArrayList<>();
        Collections.addAll(data, sightingData);
        newChildRef.setValue(data);
    }


}
