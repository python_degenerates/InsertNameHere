package python_degenerates.degenerats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by mattmcauliffe on 10/19/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class AdminHomeActivity extends AppCompatActivity {

    /**
     * this method r=provides the admin with the options for dealing with a user
     * @param savedInstanceState a bundle for creating the content view
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);

        Button logout = findViewById(R.id.admin_logout_button);
        logout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(AdminHomeActivity.this, LoginActivity.class));
            }
        });
        Button manage = findViewById(R.id.admin_manage_user_button);
        manage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(AdminHomeActivity.this, ManageUsersActivity.class));
            }
        });
    }
}
