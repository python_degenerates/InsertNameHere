package python_degenerates.degenerats;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by mattmcauliffe on 10/2/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class HomeScreenActivity extends AppCompatActivity {

    //Instance = database | reference = the start of the data base
    //you don't need a reference to any sub data in the authentication
    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    //used with progress bar. It is used to determine when we have finished loading data
    private boolean canContinue;

    /**this method creates the home screen of the app
     * which ahs a button to continue to login on it
     *
     * @param savedInstanceState this bundle is used to create the home screen
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        //in case it wasn't set before
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //getting the progress bar so you can change its visibility
        final ProgressBar progress = findViewById(R.id.home_load_progress);

        Button toLogin = (Button) findViewById(R.id.goToLoginButton);
        toLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //can continue will be set in the code below
                //start activity is passed an intent so that you can change from what you are in
                //to some different activity.
                if (canContinue)
                startActivity(new Intent(HomeScreenActivity.this, LoginActivity.class));
            }
        });

        //you can now see the progress bar
        progress.setVisibility(View.VISIBLE);
        //in the sighting data interpreter(possibly change to another class)
        //there is a list of all sightings if this list is empty then we will retrieve the data
        if (SightingManager.getSightings() == null) {
            Log.d("RefinedListStatus", "The Refined list was null");

            //a query is the main way to get data from the firebase
            //you get a child from our original data base reference
            //right now we have sightings and user children.
            //this takes awhile because it is a bunch of data
            SightingManager.loadSightings();
            canContinue = true;
            progress.setVisibility(View.INVISIBLE);
        } else {
            //if you are coming back to the homescreen after being in the app, you wont have to load
            //all of the data again
            canContinue = true;
            progress.setVisibility(View.INVISIBLE);
        }


    }
}
