package python_degenerates.degenerats;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * Created by mattmcauliffe on 10/2/17.
 */
@SuppressWarnings("CyclicClassDependency")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    //The list of newly registered users. This can be removed if we implement a database,
    //or can be stored and loaded from the database
    private static Map<String, String> emailsTypes = new HashMap<>();

    /**
     *
     * @return map of email types for login information
     */
    public static Map<String, String> getEmailsTypes() {
        return emailsTypes;
    }
    private FirebaseAuth mAuth;
    private static FirebaseUser user = null;
    private ProgressBar mProgress;


    //setter for the private firebaseuser user. in the database permissions you are able to say
    //if read and write are public. If we want only people who are logged in to be able to write
    //this IGNORE THIS COMMENT

    /**
     * this method identifies the user that is logging in
     * @param user user to login
     */
    public static void setUser(FirebaseUser user) {
        user = user;
    }

    /** this method returns the active user
     *
     * @return the current logged in user
     */
    public static FirebaseUser getUser() {
        return user;
    }


    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;




    /**this method handles the login information entered into the text boxes
     *
     * @param savedInstanceState this bundle is used to make the login screen
     */
    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        //intent from register to get email and password
        Intent fromRegister = getIntent();
        UserManager.loadUsers();

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        if (fromRegister.getStringExtra("Email") != null) {
            mEmailView.setText(fromRegister.getStringExtra("Email"));
            mPasswordView.setText(fromRegister.getStringExtra("Password"));
        }
        //written for us
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.CUPCAKE) {
            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == R.id.login || id == EditorInfo.IME_NULL) {
                        attemptLogin();
                        return true;
                    }
                    return false;
                }
            });
        }


        //when you click login this is the loading
        mProgress = findViewById(R.id.login_progress);
        mProgress.setVisibility(View.INVISIBLE);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgress.setVisibility(View.VISIBLE);

                //in this method an intent will be created and will take us to the next activity
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        Button cancel = (Button) findViewById(R.id.login_cancel_button);
        cancel.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v){
                //go back to homescreen
                startActivity(new Intent(LoginActivity.this, HomeScreenActivity.class));
            }
        });
        mAuth = FirebaseAuth.getInstance();

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */

    //towards the top mauthtask was set to null.
    // if it is not null just leave the method because it should be null and if it wasn't that means
    //that you would have already logged in so you wouldn't need to call this method
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        //the login screen will underline what you have and say that it is wrong
        //we set this to null because we don't want errors to show up if you haven't tried to log
        //in yet
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        //make strings out of what the user has typed in

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        //was a password entered in the first place and is it too short
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        // if cancel is true we wont proceed.
        //show the error if they have an error
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            //a userlogin task in a task that checks credentials in the background
            //user log in task is a method below
            mAuthTask = new UserLoginTask(email, password);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.CUPCAKE) {
                mAuthTask.execute((Void) null);
            }
        }
    }


    //these are methods that we call to check what was inputted into the email and password fields.
    public boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@") && email.contains(".");
    }

    public boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return (password.length() > 4)&illegalPasswordCharacters(password);
    }

    public boolean illegalPasswordCharacters(String password) {
        return !containsAny(password,"~?|\"\\}{/# ");
    }

    public boolean containsAny(String str, String items) {
        boolean ret = false;
        String thing;
        for (int i = 0; i < items.length(); i++) {
            thing = items.charAt(i) + "";
            ret = ret | (str.contains(thing));
        }
        return ret;
    }

    /**
     * Shows the progress UI and hides the login form.
     */

    //NOT SHIT WE ARE USING THIS IS FOR AN OLD VERSION ON ANDROID
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override

    //NOT USING
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            return new CursorLoader(this,
                    // Retrieve data rows for the device user's 'profile' contact.
                    Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                            ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                    // Select only email addresses.
                    ContactsContract.Contacts.Data.MIMETYPE +
                            " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                    .CONTENT_ITEM_TYPE},

                    // Show primary email addresses first. Note that there won't be
                    // a primary email address if the user hasn't specified one.
                    ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
        } else {
            return null;
        }
    }

    @Override

    //NOT USING
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override

    //NOT USING
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    //we aren't using contacts so this does not matter either
    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    //we are not using this either
    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    public class UserLoginTask extends AsyncTask<Void, Void, FirebaseUser> {

        private final String mEmail;
        private final String mPassword;
        private User.UserType type;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        /** checks the login information provided and logs the user in if information is
         * authentic
         *
         * @param params params string for firebase
         * @return returns the User
         */
        @Override
        protected FirebaseUser doInBackground(Void... params) {
            //sign in with user and password is a method given by firebase that we can use to check
            //credentials
            //the listener waits to hear back from the database whether or not the sign in was successful
            mAuth.signInWithEmailAndPassword(mEmail, mPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            user = mAuth.getCurrentUser();
                            setUser(user);
                            //you are looking through all of the users in your list to
                            //see which has the same email that you sent
                            //check the email against everything in firebase
                            //if this worked check the type of your user to see whether or not to log them
                            //in as an admin
                            User loggingIn = null;
                            for (User u : UserManager.getUsers()) {
                                if (u.getUserEmail().toLowerCase().equals(user.getEmail())) {
                                    type = u.getUserType();
                                    loggingIn = u;
                                }
                            }

                            //if loggingIn is null then it didn't work above
                            //if they are blocked then just dont let them in
                            if (loggingIn != null && !loggingIn.isBlocked()) {
                                //Test user type and create and execute that activity through an intent
                                if (type == null || type.equals(User.UserType.USER)) {
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                } else if (type.equals(User.UserType.ADMIN)) {
                                    startActivity(new Intent(LoginActivity.this, AdminHomeActivity.class));
                                }
                            } else {

                                //show an error
                                Toast blocked = Toast.makeText(getApplicationContext(), "Your account is blocked", Toast.LENGTH_SHORT);
                                blocked.show();
                                mProgress.setVisibility(View.INVISIBLE);
                                //take them back to the login class
                                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                            }
                            //if the task to sign in with email and password was not successful
                            //but there is still a valid email, and the email has not been used by another user
                            //then you got to the screen to sign in as a new user.
                        } else {
                            if (isEmailValid(mEmail) && !emailsTypes.containsKey(mEmail)) {
                                //intents can take a put extra which is basically something extra
                                //that you want to sent to the next class
                                //they are read in the onCreate of the next class. onCreate. getIntent will get the
                                //extras that we are currently sending or just return null
                                Intent toRegister = new Intent(LoginActivity.this, RegisterActivity.class);
                                toRegister.putExtra("Email", mEmail);
                                toRegister.putExtra("Password", mPassword);
                                startActivity(toRegister);
                            }
                        }

                    }
                });
            return user;
        }


        //you probably wont get all the way down to here.
        @Override
        protected void onPostExecute(final FirebaseUser user) {
            //set it to null for next time
            mAuthTask = null;
            //we aren't trying to log in anymore
            mProgress.setVisibility(View.INVISIBLE);
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

