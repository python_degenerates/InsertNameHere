package python_degenerates.degenerats;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by mattmcauliffe on 9/2/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class RegisterActivity extends AppCompatActivity {
    private String email;
    private String password;
    private User.UserType type;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        EditText emailBox = (EditText) findViewById(R.id.email_edit_text);
        EditText passBox = (EditText) findViewById(R.id.password_edit_text);
        //get info from the intent passed by loginActivity

        //gets the intent that started this activity
        //gets the extras from the intent and uses them to populate the boxes so you dont have to
        //retype them
        Intent input = getIntent();
        if (input.getStringExtra("Email") != null) {
            emailBox.setText(input.getStringExtra("Email"));
            passBox.setText(input.getStringExtra("Password"));
        }
        //when the register button is clicked, this retrieves the information that was entered and
        //passes it back into the login activity to be added to the user list.
        Button register = (Button) findViewById(R.id.register_button);
        register.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                name = ((EditText) findViewById(R.id.full_name_edit_text)).getText().toString();
                type = (User.UserType)((Spinner) findViewById(R.id.user_type_spinner)).getSelectedItem();
                email = ((EditText) findViewById(R.id.email_edit_text)).getText().toString();
                password = ((EditText) findViewById(R.id.password_edit_text)).getText().toString();


                //mauth function made by firebase which is passed user and password to create new user
                //this will put the user into authentication and the stuff that matt wrote inside of here
                //will also add the user to the database.
                if(DatabaseManager.createUser(email, password, name, type)) {
                    Intent passedData = new Intent(RegisterActivity.this, LoginActivity.class);
                    passedData.putExtra("Email", email);
                    passedData.putExtra("Password", password);
                    startActivity(passedData);

                } else {
                    Toast.makeText(RegisterActivity.this, "Account Creation Failed.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        Button cancel = (Button) findViewById(R.id.cancel_button);
        cancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });
        //we are still in onCreate so this all is getting the values from user type and putting them into the spinner
        //so that you can select what kind of user we are.
        Spinner user_type_spinner = (Spinner) findViewById(R.id.user_type_spinner);
        //noinspection unchecked
        ArrayAdapter<String> adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, User.UserType.values());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        user_type_spinner.setAdapter(adapter);
    }
}
