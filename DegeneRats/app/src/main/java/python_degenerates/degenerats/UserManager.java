package python_degenerates.degenerats;

import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;


/**
 *  Don't use this class yet. It's far from ready and I'm still playing around
 *  and trying to figure out how Firebase works.
 *
 *  Also its design is pretty terrible what with the `return null` stuff so yeah.
 *
 *  I still haven't fleshed out a full design yet so this will be a pretty volatile
 *  class for a while.
 */
@SuppressWarnings("CyclicClassDependency")
public class UserManager {

    private static ArrayList<User> users;

    /** this method sets new users
     *
     * @param newUsers array list of newly registered users
     */
    public static void setUsers(ArrayList<User> newUsers) {
        users = newUsers;
    }

    /** this method returns a list of users
     *
     * @return array list of users
     */
    public static ArrayList<User> getUsers() {
        return users;
    }

    /**
     * The `user` object is the main variable with which the majority
     * of the application will interact with. I don't want the app to
     * be aware of Firebase at all.
     */
    private User user;
    private FirebaseUser fbUser;

    /**
     * this method loads te users from the database
     */
    public static void loadUsers() {
        DatabaseManager.loadUsers();
    }

    /**
     * this method returns the designated user
     * @return User
     */
    public User getUser() {
        return this.user;
    }

    /**this method alters a user's type
     *
     * @param user user to have their type changed
     */
    public static void changeUserType(User user) {
        DatabaseManager.toggleUserType(user);
    }

    /**this method blocks a user's access
     *
     * @param user user
     */
    public static void blockUser(User user) {
        DatabaseManager.blockUser(user);
    }
}
