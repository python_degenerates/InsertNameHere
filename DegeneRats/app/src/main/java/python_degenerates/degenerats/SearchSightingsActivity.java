package python_degenerates.degenerats;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import java.nio.channels.GatheringByteChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
/**
 * Created by mattmcauliffe on 10/12/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class SearchSightingsActivity extends AppCompatActivity {
    private static ArrayList<Integer> searchedListIDs = null;
    private static ArrayList<int[]> searchedListDates = null;
    private static SearchSightingsActivity ref;
    private ProgressBar progress;

    protected static void setSearchedListIDs(ArrayList<Integer> list) {
        searchedListIDs = list;
    }
    protected static void setSearchedListDates(ArrayList<int[]> list) {
        searchedListDates = list;
    }

    /**
     *
     * @return array list of searched dates of rat reports
     */
    public static ArrayList<int[]> getSearchedListDates() {
        return searchedListDates;
    }


    /** This method creates the search rat sightings page with the buttons, date-pickers
     * @param savedInstanceState this bundle is used to create the view
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ref = SearchSightingsActivity.this;
        progress = findViewById(R.id.search_progress);
        if (progress != null)
            progress.setVisibility(View.INVISIBLE);

        setContentView(R.layout.activity_search_sightings);

        final EditText beginSearchID = findViewById(R.id.begin_search_id);
        final EditText endSearchID = findViewById(R.id.end_search_id);

        final DatePicker datePicker = findViewById(R.id.date_picker);
        final DatePicker endDatePicker = findViewById(R.id.end_date_picker);


        Button search = findViewById(R.id.search_all_sightings_button);
        Button graph = findViewById(R.id.display_on_graph_button);
        Button cancel = findViewById(R.id.cancel_search_button);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (progress != null)
                    progress.setVisibility(View.VISIBLE);
                int startID = -1;
                int endID = -1;
                if (beginSearchID.getText().toString().length() != 0 && endSearchID.getText().toString().length() != 0) {
                    startID = Integer.parseInt(beginSearchID.getText().toString());
                    endID = Integer.parseInt(endSearchID.getText().toString());
                }
                int month = datePicker.getMonth();
                int year = datePicker.getYear();
                int day = datePicker.getDayOfMonth();

                int eMonth = endDatePicker.getMonth();
                int eYear = endDatePicker.getYear();
                int eDay = endDatePicker.getDayOfMonth();


                int[] intParams = {startID,endID,day,month,year, eDay, eMonth, eYear, 0};
                //progress.setVisibility(View.VISIBLE);
                AsyncTask viewTask = new SearchDataTask(intParams);
                //noinspection unchecked
                viewTask.execute();



            }
        });

        graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (progress != null)
                    progress.setVisibility(View.VISIBLE);
                int startID = -1;
                int endID = -1;
                if (beginSearchID.getText().toString().length() != 0 && endSearchID.getText().toString().length() != 0) {
                    startID = Integer.parseInt(beginSearchID.getText().toString());
                    endID = Integer.parseInt(endSearchID.getText().toString());
                }
                int month = datePicker.getMonth();
                int year = datePicker.getYear();
                int day = datePicker.getDayOfMonth();
                int eMonth = endDatePicker.getMonth();
                int eYear = endDatePicker.getYear();
                int eDay = endDatePicker.getDayOfMonth();

                int[] intParams = {startID,endID,day,month,year, eDay, eMonth, eYear, 1};
                if (progress != null) {
                    progress.setVisibility(View.VISIBLE);
                }
                AsyncTask viewTask = new SearchDataTask(intParams);
                //noinspection unchecked
                viewTask.execute();



            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SearchSightingsActivity.this, MainActivity.class));
            }
        });
    }

    /**
     * this class is used to create a task that searches the rat sightings by date
     */
    private class SearchDataTask extends AsyncTask<Object, Void, ArrayList<Integer>> {
        ArrayList<Integer> searched = new ArrayList<>();
        ArrayList<int[]> dates = new ArrayList<>();

        int start_id, end_id, day, month, year, eDay, eMonth, eYear, callNum;
        List<String[]> sightings;

        public SearchDataTask(int[] intParams) {
            super();
            start_id = intParams[0];
            end_id = intParams[1];
            day = intParams[2];
            month = intParams[3];
            year = intParams[4];
            eDay = intParams[5];
            eMonth = intParams[6];
            eYear = intParams[7];
            callNum = intParams[8];
            sightings = SightingManager.getSightings();
        }

        /** This method contains the logic for identifying the sightings that
         * fit the search parameters specified by the user
         *
         * @param params unused parameter string
         * @return an array list of search rat sightings
         */
        @Override
        protected ArrayList<Integer> doInBackground(Object ... params) {
            int x = 0;
            for (String[] sighting : sightings) {
                if (!sighting[0].equals("Unique Key")) {
                    if (sighting[1].length() > 8) {
                        int id = Integer.parseInt(sighting[0]);
                        String dateString = sighting[1];
                        int item_month = Integer.parseInt(dateString.substring(0, dateString.indexOf("/")));
                        String dateStringMinusMonth = dateString.substring(dateString.indexOf("/") + 1);
                        int item_day = Integer.parseInt(dateStringMinusMonth.substring(0, dateStringMinusMonth.indexOf("/")));
                        String dateMinusMonthDay = dateStringMinusMonth.substring(dateStringMinusMonth.indexOf("/") + 1);
                        int item_year = Integer.parseInt(dateMinusMonthDay.substring(0, 4));

                        if (start_id != -1 && end_id != -1){
                            if (id >= start_id && id <= end_id) {
                                if (item_year > year && item_year < eYear) {
                                    searched.add(x);
                                    int[] dateArr = {item_day, item_month, item_year};
                                    dates.add(dateArr);
                                }
                                else if (item_year == year && item_month > month) {
                                    searched.add(x);
                                    int[] dateArr = {item_day, item_month, item_year};
                                    dates.add(dateArr);
                                }
                                else if (item_year == year && item_month == month && item_day >= day) {
                                    searched.add(x);
                                    int[] dateArr = {item_day, item_month, item_year};
                                    dates.add(dateArr);
                                }
                                else if (item_year == eYear && item_month < eMonth) {
                                    searched.add(x);
                                    int[] dateArr = {item_day, item_month, item_year};
                                    dates.add(dateArr);
                                }
                                else if (item_year == eYear && item_month == eMonth && item_day <= eDay) {
                                    searched.add(x);
                                    int[] dateArr = {item_day, item_month, item_year};
                                    dates.add(dateArr);
                                }

                            }
                        } else {
                            if (item_year > year && item_year < eYear) {
                                searched.add(x);
                                int[] dateArr = {item_day, item_month, item_year};
                                dates.add(dateArr);
                            } else if (item_year == year && item_month > month) {
                                searched.add(x);
                                int[] dateArr = {item_day, item_month, item_year};
                                dates.add(dateArr);
                            } else if (item_year == year && item_month == month && item_day >= day) {
                                searched.add(x);
                                int[] dateArr = {item_day, item_month, item_year};
                                dates.add(dateArr);
                            } else if (item_year == eYear && item_month < eMonth) {
                                searched.add(x);
                                int[] dateArr = {item_day, item_month, item_year};
                                dates.add(dateArr);
                            } else if (item_year == eYear && item_month == eMonth && item_day <= eDay) {
                                searched.add(x);
                                int[] dateArr = {item_day, item_month, item_year};
                                dates.add(dateArr);
                            }

                        }

                    }
                }
                x++;
            }
            SearchSightingsActivity.setSearchedListDates(dates);
            return searched;
        }

        /**this method creates a graph with the refined list of sightings.
         *
         * @param refined array list of refined sightings
         */
        @Override
        protected void onPostExecute(ArrayList<Integer> refined) {
            if (progress != null)
                progress.setVisibility(View.INVISIBLE);
            SearchSightingsActivity.setSearchedListIDs(refined);
            if (callNum == 0) {
                Intent viewOnMap = new Intent(ref, SightingResultsMapsActivity.class);
                viewOnMap.putExtra("SightingPositions", searchedListIDs);
                Log.d("Size of searched List", "Size of searched list: " + searched.size());
                ref.startActivity(viewOnMap);
            } else if (callNum == 1) {
                Intent viewGraph = new Intent(ref, GraphActivity.class);
                ref.startActivity(viewGraph);
            }

        }
    }
}
