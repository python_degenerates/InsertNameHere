package python_degenerates.degenerats;

import android.os.Parcelable;

/**
 * Created by mattmcauliffe on 9/28/17.
 */

public class User {
    private String full_name;
    private UserType type;
    private String user_email;
    private boolean isBlocked;

    protected enum UserType{
        USER,
        ADMIN
    }

    /**creates a new user
     *
     * @param name user's name
     * @param type user type
     * @param user_email user email
     */
    public User(String name, UserType type, String user_email) {
        this.full_name = name;
        this.type = type;
        this.user_email = user_email;
        this.isBlocked = false;
    }

    /**
     *
     * @return the string name of the user
     */
    public String getName() {
        return full_name;
    }

    /**
     *
     * @return the string email of the user
     */
    public String getUserEmail() {
        return user_email;
    }
    //keeps the password from being accessed directly

    /**
     *
     * @return the UserType of this user, either user or admin
     */
    public UserType getUserType() {
        return type;
    }

    /**
     *
     * @return boolean whether the user is blocked
     */
    public boolean isBlocked() {
        return isBlocked;
    }

    /** set's the user's blocked status
     *
     * @param status current blocked status
     */
    public void setBlocked(boolean status) {
        isBlocked = status;
    }

}
