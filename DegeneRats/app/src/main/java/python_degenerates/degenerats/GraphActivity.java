package python_degenerates.degenerats;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
/**
 * Created by mattmcauliffe on 10/28/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class GraphActivity extends AppCompatActivity {

    /**this method creates the graphview
     *
     * @param savedInstanceState bundle to create the content view with
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        ArrayList<int[]> dates = SearchSightingsActivity.getSearchedListDates();
        if (!dates.isEmpty()) {
            Map<Integer, Integer> monthsCounts = new TreeMap<>();
            for (int[] date : dates) {
                if(monthsCounts.get((12 * date[2] + date[1])) == null) {
                    monthsCounts.put((12 * date[2] + date[1]), 1);
                } else {
                    int previous = monthsCounts.get((12 * date[2] + date[1]));
                    monthsCounts.put((12 * date[2] + date[1]),previous + 1);
                }
            }
            ArrayList<DataPoint> points = new ArrayList<>();
            for(int key : monthsCounts.keySet()){
                points.add(new DataPoint(key, monthsCounts.get(key)));
            }
            DataPoint[] pointsArr = new DataPoint[points.size()];
            for (int i = 0; i < points.size(); i++) {
                pointsArr[i] = points.get(i);
            }
            for (DataPoint aPointsArr : pointsArr) {
                Log.d("Check if points", aPointsArr.toString());
            }



            GraphView graph = (GraphView) findViewById(R.id.graph);
            BarGraphSeries<DataPoint> series = new BarGraphSeries<>(pointsArr);
            graph.addSeries(series);
            series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
                @Override
                public int get(DataPoint data) {
                    return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
                }
            });

            series.setSpacing(50);
            // styling


            // draw values on top
            series.setDrawValuesOnTop(true);
            series.setValuesOnTopColor(Color.RED);
            //series.setValuesOnTopSize(50);
        }



    }
}
