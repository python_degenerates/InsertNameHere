package python_degenerates.degenerats;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
//used to turn long and lat into address -C
import android.location.Geocoder;
import android.location.Address;
import java.util.Locale;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;



/**
 * Created by mattmcauliffe on 9/2/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class ReportLocationSelection extends FragmentActivity
        implements OnMapReadyCallback {

    private GoogleMap mMap;
    Address passAddress;

    @Override
    /**
     * This method is used to create a layout for the map and its button overlay
     * @param savedInstanceState this state is used to create the view
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_location_selection);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);


        //confirm button that takes you back to the calling activity
        Button confirm = findViewById(R.id.confirm_button);
        confirm.setEnabled(false);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This method centers the map on NewYork and lets the user place down one marker at a time
     * this markers data will be use the getCompleteAddress method to convert the address of this
     * marker and send it off to report rat sighting activity.
     * @param googleMap this is an object upon which the markers can be placed to receive location
     *                  data
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(9);
        LatLng newYork = new LatLng(40.7128, -74.006);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(newYork));
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
            // TODO Auto-generated method stub
            mMap.clear();
            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(point.latitude, point.longitude)).title("Sighting");

            mMap.addMarker(marker);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(point));
            passAddress = getCompleteAddress(point);
            Button confirm = findViewById(R.id.confirm_button);
            confirm.setEnabled(true);
            if(confirm.isEnabled()) {
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent passedData = new Intent(
                                ReportLocationSelection.this, ReportRatSightingActivity.class);
                        passedData.putExtra("Address", passAddress);
                        startActivity(passedData);
                    }
                });
            }
            }
        });
    }

    /**
     * This method takes in a lat long object whose long and lat data are converted into an Address
     * object containing more specific information about that coordinate position.
     * @param location a lat long object which contains the long and lat of a map marker for a rat
     *                 sighting
     * @return the address object containing more specific information about the location in the
     * long lat object.
     */
    public Address getCompleteAddress(LatLng location) {
        @SuppressWarnings("UnusedAssignment") String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        Address returnedAddress = null;
        try {
            List<Address> addresses = geocoder.getFromLocation(
                    location.latitude, location.longitude, 1);

            if (addresses != null) {
                StringBuilder strReturnedAddress = new StringBuilder("");
                returnedAddress = addresses.get(0);
                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                //noinspection UnusedAssignment
                strAdd = strReturnedAddress.toString();
                Log.w("Hi matt", strReturnedAddress.toString());

            } else {
                Log.w("My Current address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current address", "Cannot get Address!");
        }
        return returnedAddress;
    }
}

