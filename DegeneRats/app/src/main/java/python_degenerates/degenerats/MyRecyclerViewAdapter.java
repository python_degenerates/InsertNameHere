package python_degenerates.degenerats;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mattmcauliffe on 9/30/17.
 */

@SuppressWarnings("CyclicClassDependency")
public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.CustomViewHolder> {
    private List<String[]> itemList;
    @SuppressWarnings("FieldCanBeLocal")
    private Context mContext;
    private RecyclerView mRecyclerView;

    /**
     *
     * @param context context used to create view
     * @param itemList item list of items to be in the view
     */
    public MyRecyclerViewAdapter(Context context, List<String[]> itemList) {
        this.itemList = itemList;
        this.mContext = context;
    }

    /**This method creates the recycler view
     *
     * @param viewGroup view group to be used in our custom holder
     * @param i parameter for onCreateViewHolder method, not used
     * @return our custom view holder for display
     */
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycle_view_row_layout, null);
        mRecyclerView = (RecyclerView) viewGroup;
        return new MyRecyclerViewAdapter.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        String[] item = itemList.get(i);
        String info = "";
        for (String part : item) {
            info += part + "; ";
        }
        //Setting text view title
        customViewHolder.textView.setText(info);
        customViewHolder.itemView.setOnClickListener(new MyOnClickListener());
    }

    @Override
    public int getItemCount() {
        return (null != itemList ? itemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView textView;

        public CustomViewHolder(View view) {
            super(view);
            this.textView = (TextView) view.findViewById(R.id.title);
        }
    }

    private class MyOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(final View view) {
            int itemPosition = mRecyclerView.getChildLayoutPosition(view);
            Intent intent = new Intent(view.getContext(), SightingDetailScreenActivity.class);
            intent.putExtra("Position", itemPosition);
            view.getContext().startActivity(intent);
        }

    }
}
