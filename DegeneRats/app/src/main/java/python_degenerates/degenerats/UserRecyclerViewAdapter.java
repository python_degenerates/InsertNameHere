package python_degenerates.degenerats;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mattmcauliffe on 10/2/17.
 */

@SuppressWarnings("CyclicClassDependency")
public class UserRecyclerViewAdapter extends RecyclerView.Adapter<UserRecyclerViewAdapter.CustomViewHolder> {
    private List<User> itemList;
    @SuppressWarnings("FieldCanBeLocal")
    private Context mContext;
    private RecyclerView mRecyclerView;

    /**
     *
     * @param context context for recycler view
     * @param itemList list of items to be displayed
     */
    public UserRecyclerViewAdapter(Context context, List<User> itemList) {
        this.itemList = itemList;
        this.mContext = context;
    }

    /**
     *
     * @param viewGroup the viewgroup whose context will be used
     * @param i unused
     * @return this method returns a custom view holder
     */
    @Override
    public UserRecyclerViewAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycle_view_user_layout, null);
        mRecyclerView = (RecyclerView) viewGroup;
        return new UserRecyclerViewAdapter.CustomViewHolder(view);
    }

    /**
     * this method creates and sets the onclicklistener
     * @param customViewHolder generated for the user
     * @param i unused
     */
    @Override
    public void onBindViewHolder(UserRecyclerViewAdapter.CustomViewHolder customViewHolder, int i) {
        User user = itemList.get(i);
        customViewHolder.userText.setText("Name: " + user.getName() + "\nEmail: " + user.getUserEmail()
                + "\nType: " + user.getUserType().toString() + "\nBlocked: " + user.isBlocked());

        customViewHolder.itemView.setOnClickListener(new UserOnClickListener());
    }

    /**
     *
     * @return item count if size >0
     */
    @Override
    public int getItemCount() {
        return (null != itemList ? itemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView userText;

        public CustomViewHolder(View view) {
            super(view);
            this.userText = view.findViewById(R.id.user);
        }
    }

    private class UserOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(final View view) {
            int itemPosition = mRecyclerView.getChildLayoutPosition(view);
            Intent intent = new Intent(view.getContext(), UserDetailScreenActivity.class);
            intent.putExtra("Position", itemPosition);
            view.getContext().startActivity(intent);
        }

    }
}
