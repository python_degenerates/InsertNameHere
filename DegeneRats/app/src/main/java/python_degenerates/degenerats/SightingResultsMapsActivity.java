package python_degenerates.degenerats;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.io.FileOutputStream;
import java.util.ArrayList;
/**
 * Created by mattmcauliffe on 9/2/17.
 */
public class SightingResultsMapsActivity extends FragmentActivity implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback {

    @SuppressWarnings("FieldCanBeLocal")
    private GoogleMap mMap;
    private ArrayList<Integer> sightingPositions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sighting_results_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        sightingPositions = intent.getIntegerArrayListExtra("SightingPositions");
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        if (!sightingPositions.isEmpty()) {
            String[] sighting = SightingManager.getSightings().get(1);
            if(sighting.length > 7) {
                LatLng startPos = new LatLng(Double.parseDouble(sighting[7]), Double.parseDouble(sighting[8]));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(startPos));
                mMap.setMinZoomPreference(9);
            }
        }
        for (Integer i : sightingPositions) {
            String[] sighting = SightingManager.getSightings().get(i);
            if (sighting.length > 7) {
                if (sighting[7].length() > 0) {
                    LatLng position = new LatLng(Double.parseDouble(sighting[7]), Double.parseDouble(sighting[8]));
                    Marker marker = mMap.addMarker(new MarkerOptions().position(position).title("SightingID: " + sighting[0])
                            .snippet("Time: " + sighting[1] + "\n Location: " + sighting[4] + " " + sighting[5] + ", " + sighting[6]));
                    marker.setTag(sighting);
                    marker.setAlpha(0.8f);
                    marker.setIcon(BitmapDescriptorFactory.fromAsset("rat icon.bmp"));

                    mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {
                            Context mContext = getApplicationContext();
                            LinearLayout info = new LinearLayout(mContext);
                            info.setOrientation(LinearLayout.VERTICAL);

                            TextView title = new TextView(mContext);
                            title.setTextColor(Color.BLACK);
                            title.setGravity(Gravity.CENTER);
                            title.setText(marker.getTitle());

                            TextView snippet = new TextView(mContext);
                            snippet.setTextColor(Color.GRAY);
                            snippet.setText(marker.getSnippet());

                            info.addView(title);
                            info.addView(snippet);

                            return info;
                        }
                    });
                }

            }
        }
    }
    @Override
    public boolean onMarkerClick(final Marker marker) {

        // Retrieve the data from the marker.
        String[] sightingData = (String[]) marker.getTag();

        // Check if a click count was set, then display the click count.
        if (sightingData != null) {
            Toast.makeText(this, "ID: " + sightingData[0]
                    + "\nLocation Type: " + sightingData[2]
                    + "\nIncident Address: " + sightingData[4]
                    + "\nBorough: " + sightingData[6], Toast.LENGTH_LONG).show();
        }

        // Return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return false;
    }
}
