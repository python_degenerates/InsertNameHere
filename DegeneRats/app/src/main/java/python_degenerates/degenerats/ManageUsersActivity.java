package python_degenerates.degenerats;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
/**
 * Created by mattmcauliffe on 10/2/17.
 */
@SuppressWarnings("CyclicClassDependency")
public class ManageUsersActivity extends AppCompatActivity {

    @SuppressWarnings("FieldCanBeLocal")
    private RecyclerView mRecyclerView;
    @SuppressWarnings("FieldCanBeLocal")
    private UserRecyclerViewAdapter adapter;

    /**this method creates the view for the admin to manage the registered users
     *
     * @param savedInstanceState this bundle is used to create the content view
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_users);
        for (String email : LoginActivity.getEmailsTypes().keySet()){
            Log.d("user list debug", email);
        }

        mRecyclerView = findViewById(R.id.recycler_view_users);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new UserRecyclerViewAdapter(ManageUsersActivity.this, UserManager.getUsers());
        mRecyclerView.setAdapter(adapter);

        Button back = (Button) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(ManageUsersActivity.this, AdminHomeActivity.class));
            }
        });
    }
}
