package python_degenerates.degenerats;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.github.clans.fab.FloatingActionButton;

/**
 * Created by mattmcauliffe on 9/2/17.
 */

public class MainActivity extends AppCompatActivity {
    @SuppressWarnings("FieldCanBeLocal")
    private RecyclerView mRecyclerView;
    @SuppressWarnings("FieldCanBeLocal")
    private MyRecyclerViewAdapter adapter;
    @SuppressWarnings("FieldCanBeLocal")
    private String userName;

    /**
     * this method organizes the sightings list for the main activity screen
     * @param savedInstanceState this state is used to create the view
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Rat Reports");

        Intent input = getIntent();
        userName = input.getStringExtra("User");

        mRecyclerView = findViewById(R.id.recycler_view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
        adapter = new MyRecyclerViewAdapter(MainActivity.this, SightingManager.getSightings());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            mRecyclerView.setAdapter(adapter);
        }



        FloatingActionButton toLogin = findViewById(R.id.logout_button);
        FloatingActionButton toReport = findViewById(R.id.report_sighting_button);
        FloatingActionButton toSearch = findViewById(R.id.search_sightings_button);

        toLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                startActivity(new Intent(MainActivity.this, HomeScreenActivity.class));
            }
        });
        toReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                startActivity(new Intent(MainActivity.this, ReportRatSightingActivity.class));
            }
        });
        toSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent search = new Intent(MainActivity.this, SearchSightingsActivity.class);
                startActivity(search);
            }
        });
    }
}
