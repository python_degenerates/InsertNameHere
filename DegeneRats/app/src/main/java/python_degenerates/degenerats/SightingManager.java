package python_degenerates.degenerats;

import java.util.ArrayList;

/**
 * Created by mattmcauliffe on 10/24/17.
 */

@SuppressWarnings({"CyclicClassDependency", "UtilityClass"})
public class SightingManager {
    private static ArrayList<String[]> sightings;
    private static int maxID = 0;

    /**
     *
     * @param newSightings sets new sightings list to be added
     */
    public static void setSightings(ArrayList<String[]> newSightings) {
        sightings = newSightings;
    }

    /**
     *
     * @return returns array-list of the sightings
     */
    public static ArrayList<String[]> getSightings() {
        return sightings;
    }

    /**
     * loads sightings from the database
     */
    public static void loadSightings() {
        DatabaseManager.loadSightings();
    }

    /**this method adds a new sighting to the database given its information
     *
     * @param newSighting takes in an array of data for a new sighting
     */
    public static void writeSightingData(String[] newSighting) {
        DatabaseManager.writeSightingData(newSighting);
    }

    /**
     *
     * @param id this ID is for the unique ID parameter of a rat sighting
     */
    public static void setMaxID(int id) {
        maxID = id;
    }

    /**
     *
     * @return returns the int value of the maximum unique ID
     */
    public static int getMaxID() {
        return maxID;
    }

    /**
     * this method increments the maximum ID to keep track of the rat sightings
     */
    public static void incrementMaxID() {
        maxID++;
    }
}
